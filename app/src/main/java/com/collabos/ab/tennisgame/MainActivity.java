package com.collabos.ab.tennisgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
Button clickButton;
    EditText p1Name,p2Name;
    String player1,player2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        clickButton = (Button) findViewById(R.id.button);
        clickButton.setOnClickListener( new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                getUi();

                Log.d("Examplae ","button clicked ");
                Intent myIntent = new Intent(MainActivity.this, GameActivity.class);
                myIntent.putExtra("p1Name", player1);
                myIntent.putExtra("p2Name", player2);

                MainActivity.this.startActivity(myIntent);
            }
        });
        //TODO create tennis game
        // Write a program that simulates a Tennis game. Given 2 players,
        // randomly assign points to one of the players, then call the score
        // (Thirty-Love, Deuce, Advantage Player 1, etc.) each time. For example,
        // when the game starts, the score is Love-All. If Player 1 scores after
        // the first round, the second score shown is Fifteen-Love. If Player 2 scores after
        // the second round, the third score shown is Fifteen-All. If the game continues this
        // way and both players get 4 points apiece, the score is Deuce. In other words,
        // a random game’s output could be an array of Love-All, Fifteen-Love, Thirty-Love,
        // Thirty-Fifteen, Thirty-All, Advantage Player 1, Deuce, Advantage Player 2, Win for
        // Player 2. Obviously, there are a lot of other options for the game to unfold. If
        // you’re not familiar with the scoring method for Tennis, simply google it and you will
        // find what you need.

    }
    private void getUi(){

        p1Name=(EditText)findViewById(R.id.editTextP1);
        p2Name=(EditText)findViewById(R.id.editTextP2);
        player1=p1Name.getText().toString();
        player2=p2Name.getText().toString();
    }
}
