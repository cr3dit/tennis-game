package com.collabos.ab.tennisgame;

/**
 * Created by AB on 26.08.2017..
 */


import java.util.Random;

public class Score {
    int[] game = new int[2];
    private int[] point = new int[2];
    private boolean gameInProgress = true;
    private String winner;

    private String[] textScr = new String[]{"Love", "Fifteen", "Thirty", "Forty", "Game", "Deuce", "Advantage Player 1","Advantage Player 2", "-All"};

    public boolean isGameInProgress() {
        return gameInProgress;
    }

    public int getGame(int base) {
        return game[base];
    }

    public void incrementPoint() {
        Random r = new Random();
        int low = 0;
        int high = 2;
        int x = r.nextInt(high - low) + low;
        point[x] = point[x] + 1;

        if ((point[x] == 4) && (point[Math.abs(x - 1)] < 3)) {

            game[x] = game[x] + 1;

            if (game[x] == 6) {
                x=x+1;
                winner = "Game, Set, Match Player "+x;
                gameInProgress = false;
            }

            point[0] = 0;
            point[1] = 0;

        } else if ((point[x] > 4) && (point[x] - 1 > point[Math.abs(x - 1)])) {
            game[x] = game[x] + 1;
            if (game[x] == 6) {
                x=x+1;
                winner = "Game, Set, Match Player "+x;
                gameInProgress = false;
            }

            point[0] = 0;
            point[1] = 0;
       }
    }


    public String textScore() {


        if (( point[0]>= 3) && (point[1] == point[0])) {


            return textScr[5];
            //deuce
        } else if ((point[0] == 0) && (point[1] == 0)) {


            return textScr[0] + textScr[8];
            //initial score
        } else if ((point[0] == point[1]) && (point[0] < 3)) {


            return textScr[point[0]] + textScr[8];
        } else if (!gameInProgress) {

            return winner;
        }else if ((point[0] <4) && (point[1] < 4)) {


            return textScr[point[0]]  + "-" + textScr[point[1]];

        }else if ((point[0] >3) && (point[0] >point[1])) {


            return textScr[6];

        }
        else if ((point[1] >3) && (point[1] >point[0])) {


            return textScr[7];

        }
        else {

            return point[0] + "-" + point[1];
        }

    }


}
