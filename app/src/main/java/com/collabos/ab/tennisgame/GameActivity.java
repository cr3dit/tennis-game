package com.collabos.ab.tennisgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by AB on 26.08.2017..
 */

public class GameActivity extends AppCompatActivity {
TextView text,p1Game,p2Game, announceTextView;
    Player p1;
    Player p2;

    Score score;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
        Intent intent = getIntent();
        score=new Score();
        p1=new Player(intent.getStringExtra("p1Name"));
        p2=new Player(intent.getStringExtra("p2Name"));



        text=(TextView) findViewById(R.id.anounceTV);
        p1Game=(TextView) findViewById(R.id.p1Game);
        p2Game=(TextView) findViewById(R.id.p2Game);
        announceTextView =(TextView)findViewById(R.id.score);

        text.setText(p1.getName()+" vs "+p2.getName());

        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (score.isGameInProgress()) {

                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                p1Game.setText(score.getGame(0)+" ");
                                p2Game.setText(" "+score.getGame(1));

                                score.incrementPoint();

                                announceTextView.setText(score.textScore());

                            }
                        });
                    }
                } catch (InterruptedException e) { }
            }
        };

        t.start();


       }

    }


        //TODO create tennis game
        // Write a program that simulates a Tennis game. Given 2 players,
        // randomly assign points to one of the players, then call the score
        // (Thirty-Love, Deuce, Advantage Player 1, etc.) each time. For example,
        // when the game starts, the score is Love-All. If Player 1 scores after
        // the first round, the second score shown is Fifteen-Love. If Player 2 scores after
        // the second round, the third score shown is Fifteen-All. If the game continues this
        // way and both players get 4 points apiece, the score is Deuce. In other words,
        // a random game’s output could be an array of Love-All, Fifteen-Love, Thirty-Love,
        // Thirty-Fifteen, Thirty-All, Advantage Player 1, Deuce, Advantage Player 2, Win for
        // Player 2. Obviously, there are a lot of other options for the game to unfold. If
        // you’re not familiar with the scoring method for Tennis, simply google it and you will
        // find what you need.

